#Binary brewing official repo

This is the main portal for the Brooklyn based micro brewery, Binary Brewing. 

Based in the old Federal Brewing Co. building in Boerum Hill, Brooklyn, Adam Grealish and Charlie Mydlarz have been crafting heady brews since early 2014.

The impetus behind these brews is simply that of "lets make what we like to drink". The early batches were heavily hopped versions of American IPAs, Wheat Beers and Ales, with more recent Seasonal Stouts making an appearance.

This repo houses all of the breweries artwork and recipes